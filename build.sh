#!/usr/bin/env bash

. ./common.sh

getOS
if [ $? -ne 0 ]; then exit $?; fi

getVersion
if [ $? -ne 0 ]; then exit $?; fi

echo "Building AS11 version $VERSION"

ERR=0
case $OS in
    win32)  MSBUILD=`find $WINDIR/Microsoft.NET -iname msbuild.exe | \
                        grep -iv gac | \
                        grep -iv framework64 | \
                        sort -r | \
                        head -n 1`
            checkError $? "Failed to find msbuild.exe. Do you have VS2013 installed?" || exit $?

            if [ -z $MSBUILD ]
            then
                checkError 1 "Failed to find msbuild.exe. Do you have VS2013 installed?" || exit $?
            fi

            VCXPROJ=src/win32/as11.vcxproj

            echo "Executing $MSBUILD $VCXPROJ..."
            $MSBUILD $VCXPROJ
            checkError $? "Failed to build $VCXPROJ. Do you have VS2013 installed?" || exit $?
            ;;

    linux | macos)
            cd src
			make -B
            checkError $? "Failed to build" || exit $?
            ;;

    *)      checkError 1 'Internal script error!' || exit $?
            ;;
esac

echo "SUCCEEDED"
exit 0
