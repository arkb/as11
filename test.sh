#!/usr/bin/env bash

. ./common.sh

function deleteFile ()
{
	rm $1
    checkError $? "Failed to remove $1"
}

function testFile ()
{
	local INF=$1
	local DIR=`dirname $INF`
	local NAME=`basename $INF .asm`
	local OUTF="$DIR/$NAME.s19"
	local EXPF="$DIR/$NAME.exp"
    local LSTF="$DIR/$NAME.lst"
    local EXPLSTF="$DIR/$NAME.lst.exp"

	if [ -e $OUTF ]
	then
		deleteFile $OUTF || return $?
	fi

    if [ -e $LSTF ]
    then
        deleteFile $LSTF || return $?
    fi

	echo "Processing $INF -> ($OUTF, $LSTF) ..."
	$TESTAPP $INF -l > $LSTF
    checkError $? "Failed to process $INF" || return $?

	echo "Comparing $OUTF with $EXPF ..."
	diff $EXPF $OUTF
    checkError $? "Failed to verify $INF output" || return $?

    echo "Comparing $LSTF with $EXPLSTF ..."
    diff $EXPLSTF $LSTF
    checkError $? "Failed to verify $INF listing" || return $?

	deleteFile $OUTF
    deleteFile $LSTF
}

function reportResult ()
{
    local error=$1

    echo
    echo "***************************************************"
    if [ $error -eq 0 ]
    then
        echo "SUCCEEDED"
    else
        echo "FAILED"
    fi
    echo "***************************************************"

    return $error
}

error=0

getOS || reportResult $? || exit $?

TESTAPP=

case $OS in
    win32)
        TESTAPP=$PWD/src/win32/Debug/as11.exe
        ;;

    linux | macos)
        TESTAPP=src/as11
        ;;

    macos)
        TESTAPP=src/as11
        ;;

    *)  checkError 1 'Internal script error!' || reportResult $? || exit $?
        ;;
esac

echo "Testing $TESTAPP under $OS"

cd ./tests

for i in *.asm
do
	testFile $i || error=1
done

cd -

reportResult $error
exit $?
