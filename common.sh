# sed options that are universal on all supported platforms.
# NOTE: Assumes git's bash environment for Windows.
#
SED="sed -E -n"

# uname options that are universal on all supported platforms.
# NOTE: Assumes git's bash environment for Windows.
#
UNAME="uname -s"

function checkError ()
{
    local err=$1
    local msg=$2

    if [ $err -ne 0 ]
    then
        echo "FAILURE: $msg (error: $err)"
    fi

    return $err
}

function getOS ()
{
    local IS_WINDOWS=`$UNAME | $SED -e 's/^MINGW32.*$/1/ p'`
    local IS_LINUX=`$UNAME | $SED -e 's/^Linux.*$/1/ p'`
    local IS_MACOS=`$UNAME | $SED -e 's/^Darwin.*$/1/ p'`

    IS_WINDOWS=${IS_WINDOWS:-0}
    IS_LINUX=${IS_LINUX:-0}
    IS_MACOS=${IS_MACOS:-0}

    if [ $(($IS_WINDOWS + $IS_LINUX + $IS_MACOS)) -ne 1 ]
    then
        echo "Can't figure out the OS type:"
        echo "  IS_WINDOWS = $IS_WINDOWS"
        echo "  IS_LINUX = $IS_LINUX"
        echo "  IS_MACOS = $IS_MACOS"
        exit 1
    fi

    OS=
    if [ $IS_WINDOWS -gt 0 ]; then OS=win32; fi
    if [ $IS_LINUX -gt 0 ]; then OS=linux; fi
    if [ $IS_MACOS -gt 0 ]; then OS=macos; fi
}

function getVersion ()
{
    local VERSION_H="src/version.h"

    VERSION_MAJOR=`$SED -e 's/.*MAJOR[[:space:]]+(.*)$/\1/ p' $VERSION_H`
    checkError $? "Failed to parse $VERSION_H" || return $?

    VERSION_MINOR=`$SED -e 's/.*MINOR[[:space:]]+(.*)$/\1/ p' $VERSION_H`
    checkError $? "Failed to parse $VERSION_H" || return $?

    VERSION="$VERSION_MAJOR.$VERSION_MINOR"
}