0001                         **************************************************************************
0002                         * Software support routines for the MC68HC68T1 Real-Time-Clock/RAM & the *
0003                         * EVBU demonstration board.  Routines include...			 *
0004                         * RTCINIT - Subroutine to initialize Port D and SPI system.		 *
0005                         * SETDFLT - Subroutine to set up a default time 12:00:00 Mon 1/1/1989	 *
0006                         *	    and alarm set to same time but turned off.	RTC turned on.	 *
0007                         * SETTIME - Subroutine to set time sec/min/hr as pointed to by Y-reg	 *
0008                         * SETDATE - Subroutine to set date dow/dom/month/yr as pointed to by Y	 *
0009                         * SETALRM - Subroutine to set alarm time sec/min/hr as pointed to by Y	 *
0010                         * W1RTC   - Subroutine to write A data to addr B in RTC 		 *
0011                         * R1RTC   - Subroutine to read A data from addr B in RTC		 *
0012                         * WBURST  - Burst write B bytes starting at addr A in RTC, uses Y index  *
0013                         * RBURST  - Burst raed B bytes starting at addr A in RTC, uses Y index	 *
0014                         * DISPTIM - Display current time (alternate entry to do CR,LF first)	 *
0015                         **************************************************************************
0016                         
0017                         *****
0018                         * 68HC11 Register Equates
0019                         *****
0020                         
0021 1000                    REGBAS	EQU	$1000	Base address for register block of HC11
0022                         
0023 0008                    PORTD	EQU	$08	Port D (offset from REGBAS)
0024 0020                    SSPIN	EQU	$20	Port D bit 5 is SS* pin
0025 0009                    DDRD	EQU	$09	Data direction register for port D
0026                         * 0,0,SS*,SCK;MOSI,MISO,TxD,RxD (RxD & TxD direction controlled by SCI)
0027                         * For EVBU w/ RTC use $38 -> SS,SCK,and MOSI=OUTS, rest=INS
0028                         
0029 0028                    SPCR	EQU	$28	SPI control register
0030                         * SPIE,SPE,DWOM,MSTR;CPOL,CPHA,SPR1,SPR0 (set to $54 for RTC interface)
0031                         
0032 0029                    SPSR	EQU	$29	SPI status register
0033                         * SPIF,WCOL,-,MODF;-,-,-,-
0034 0080                    SPIF	EQU	$80	Mask for SPIF flag bit
0035                         
0036 002a                    SPDR	EQU	$2A	SPI data register
0037                         
0038                         *****
0039                         * RTC Register Equates
0040                         *****
0041                         
0042 0000                    RTCRAM	EQU	$00	RAM in RTC is from $00 to $1F (32 bytes)
0043                         
0044 0020                    RTCSEC	EQU	$20	Two BCD digits 00-59
0045 0021                    RTCMIN	EQU	$21	Two BCD digits 00-59
0046 0022                    RTCHR	EQU	$22	Two BCD digits 01-12 or (00-23 if 24 hr mode)
0047                         *  Add $80 for 12 hr AM, Add $A0 for 12 hr PM
0048 0080                    AM	EQU	$80	Offset constant to set HRs to 12 hr AM
0049 00a0                    PM	EQU	$A0	Offset constant to set HRs to 12 hr PM
0050                         
0051 0023                    WEEKDAY EQU	$23	One BCD digit 01-07 (Sunday = 01)
0052 0024                    MODATE	EQU	$24	Two BCD digits 01-31
0053 0025                    MONTH	EQU	$25	Two BCD digits 01-12 (January = 01)
0054 0026                    YEAR	EQU	$26	Two BCD digits 00-99
0055                         
0056 0028                    ALRMSEC EQU	$28	Two BCD digits 00-59
0057 0029                    ALRMMIN EQU	$29	Two BCD digits 00-59
0058 002a                    ALRMHR	EQU	$2A	Two BCD digits 01-12 or (00-23 if 24 hr mode)
0059                         *  Add $80 for 12 hr AM, Add $A0 for 12 hr PM
0060                         
0061 0030                    RTCSTAT EQU	$30	RTC status register (read only)
0062                         * 0,WDOG,0,FRSTUP;INTHI,PWRSEN,ALRMINT,CLKINT
0063                         
0064 0031                    RTCCTRL EQU	$31	RTC clock control register
0065                         * START,LINE,XTLSEL1,XTLSEL0;50/60,CLKOUT2,CLKOUT1,CLKOUT0
0066                         * Set RTCCTRL = $B0 for Clk ON, 32KHz crystal, 60Hz, CLK OUT = Xtal
0067                         
0068 0032                    RTCINT	EQU	$32	RTC interrupt control register
0069                         * WDOGEN,PWRDWN,PWRSENE,ALRMEN;4-Bit PERIOD SELECT (see RTC data sheet)
0070                         
0071                         *****
0072                         * Buffalo 3.2 Equates and Jump Table addresses
0073                         *****
0074                         
0075 0047                    USTACK	EQU	$47	Top of user stack area (0033-0047) Buffalo 3.2
0076 0030                    TIMTMP	EQU	$30	3 bytes Temp holding locations for time display
0077 ff7c                    WARMST	EQU	$FF7C	Warm Start
0078 ffaf                    OUTPUT	EQU	$FFAF	Output ASCII in A-reg to EVBU terminal screen
0079 ffbb                    OUT1BYT EQU	$FFBB	Convert & display byte pointed-to by X
0080 ffbe                    OUT1BSP EQU	$FFBE	Like OUT1BYT with extra trailing space
0081 ffc4                    OUTCRLF EQU	$FFC4	Output a <CR> and <LF>
0082                         
0083                         * End of Equates
0084                         *****
0085                         
0086 0100                    	ORG	$0100	Upper half of 'E9 RAM
0087                         
0088                         **************************************************************************
0089                         * RTCINIT - Subroutine to initialize Port D and SPI system.		 *
0090                         **************************************************************************
0091 0100 8e 00 47           RTCINIT LDS	#USTACK Initialize stack pointer
0092 0103 ce 10 00           	LDX	#REGBAS Point at start of register block
0093 0106 86 00              	LDAA	#$00
0094 0108 a7 08              	STAA	PORTD,X Initial port D data
0095 010a 86 38              	LDAA	#$38	SS,SCK,and MOSI=OUTS, others=INS
0096 010c a7 09              	STAA	DDRD,X	Set directions for port D pins
0097 010e 86 54              	LDAA	#$54	SPIE,SPE,DWOM,MSTR;CPOL,CPHA,SPR1,SPR0
0098 0110 a7 28              	STAA	SPCR,X	SPI On, Master, CPOL:CPHA=0:1, 1MHz
0099 0112 39                 	RTS		** Return from RTCINIT **
0100                         
0101                         **************************************************************************
0102                         * SETDFLT - Subroutine to set up a default time 12:00:00 Mon 1/1/1989	 *
0103                         *	    and alarm set to same time but turned off.	RTC turned on.	 *
0104                         * SETOTHR - This is alt entry point to set time to that pointed-to by Y  *
0105                         *	    Y must point to ordered set of 11 bytes (see DFLTSEC as Ex)  *
0106                         **************************************************************************
0107 0113 18 ce 01 3d        SETDFLT LDY	#DFLTSEC  Point at data to set RTC for defaults
0108 0117 86 20              SETOTHR LDAA	#RTCSEC Addr of first clock data loc in RTC
0109 0119 c6 0b              	LDAB	#11	Number of bytes to burst transfer to RTC
0110 011b 8d 49              	BSR	WBURST	Do burst transfer of default settings
0111 011d 86 b0              	LDAA	#$B0	RTC Clk ON, 32KHz crystal, 60Hz, CLK OUT = Xtal
0112 011f c6 31              	LDAB	#RTCCTRL  Address for RTC control reg
0113 0121 8d 25              	BSR	W1RTC	Transfer init control word to start RTC
0114 0123 86 00              	LDAA	#$00	RTC Clk ON, 32KHz crystal, 60Hz, CLK OUT = Xtal
0115 0125 c6 32              	LDAB	#RTCINT  Address for RTC interrupt control reg
0116 0127 8d 1f              	BSR	W1RTC	Turn off RTC interrupts
0117 0129 39                 	RTS		** Return from SETDFLT or SETOTHR **
0118                         
0119                         **************************************************************************
0120                         * SETTIME - Subroutine to set time sec/min/hr as pointed to by Y-reg	 *
0121                         **************************************************************************
0122 012a 86 20              SETTIME LDAA	#RTCSEC Address of first byte of burst Sec/Min/Hr
0123 012c c6 03              	LDAB	#3	Number of bytes to burst transfer
0124 012e 20 0a              	BRA	BRSTOUT Go to common exit point
0125                         
0126                         **************************************************************************
0127                         * SETDATE - Subroutine to set date dow/dom/month/yr as pointed to by Y	 *
0128                         **************************************************************************
0129 0130 86 23              SETDATE LDAA	#WEEKDAY  Address of 1st byte of burst WkDay/MoDat/Mo/Yr
0130 0132 c6 04              	LDAB	#4	Number of bytes to burst transfer
0131 0134 20 04              	BRA	BRSTOUT Go to common exit point
0132                         
0133                         **************************************************************************
0134                         * SETALRM - Subroutine to set alarm time sec/min/hr as pointed to by Y	 *
0135                         **************************************************************************
0136 0136 86 28              SETALRM LDAA	#ALRMSEC  Address of first byte of burst Sec/Min/Hr
0137 0138 c6 03              	LDAB	#3	Number of bytes to burst transfer
0138 013a 8d 2a              BRSTOUT BSR	WBURST	Burst transfer to RTC
0139 013c 39                 	RTS		** Return from SETALRM, SETDATE or SETTIME **
0140                         
0141 013d 00 00 92           DFLTSEC FCB	$00,$00,$12+AM	12:00:00 AM
0142 0140 01 01 01 89        DFLTDAT FCB	$01,$01,$01,$89  Sun, 01/01/89
0143 0144 00                 	FCB	$00	Place holder for unused loc $27 in RTC
0144 0145 00 00 92           DFLALRM FCB	$00,$00,$12+AM	12:00:00 AM
0145                         
0146                         
0147                         **************************************************************************
0148                         * W1RTC   - Subroutine to write A data to addr B in RTC 		 *
0149                         **************************************************************************
0150                         * R1RTC   - Subroutine to read A data from addr B in RTC		 *
0151                         **************************************************************************
0152 0148 ca 80              W1RTC	ORAB	#$80	Set MSB of Addr byte to indicate write
0153 014a 3c                 R1RTC	PSHX		Save X for now
0154 014b ce 10 00           	LDX	#REGBAS Point to start of register block
0155 014e 1c 08 20           	BSET	PORTD,X SSPIN  Enable SS pin of RTC
0156 0151 e7 2a              	STAB	SPDR,X	Write Addr byte to RTC
0157 0153 1f 29 80 fc        	BRCLR	SPSR,X SPIF *  Wait for SPIF
0158 0157 a7 2a              	STAA	SPDR,X	Initiate xfer of A to RTC
0159 0159 1f 29 80 fc        	BRCLR	SPSR,X SPIF *  Wait for SPIF
0160 015d a6 2a              	LDAA	SPDR,X	Get data from RTC into A
0161 015f 1d 08 20           	BCLR	PORTD,X SSPIN  Disable SS pin of RTC
0162 0162 c4 7f              	ANDB	#$7F	Restore original value to B
0163 0164 38                 	PULX		Restore X
0164 0165 39                 	RTS		** Return from W1RTC or R1RTC **
0165                         
0166                         **************************************************************************
0167                         * WBURST  - Burst write B bytes starting at addr A in RTC, uses Y index  *
0168                         **************************************************************************
0169 0166 8a 80              WBURST	ORAA	#$80	Set MSB of Addr byte to indicate write
0170 0168 3c                 	PSHX		Save X for now
0171 0169 ce 10 00           	LDX	#REGBAS Point to start of register block
0172 016c 1c 08 20           	BSET	PORTD,X SSPIN  Enable SS pin of RTC
0173 016f a7 2a              	STAA	SPDR,X	Write Addr byte to RTC
0174 0171 1f 29 80 fc        	BRCLR	SPSR,X SPIF *  Wait for SPIF
0175 0175 18 a6 00           MOREW	LDAA	0,Y	Get next data byte to transfer
0176 0178 a7 2a              	STAA	SPDR,X	Initiate xfer of A to RTC
0177 017a 1f 29 80 fc        	BRCLR	SPSR,X SPIF *  Wait for SPIF
0178 017e a6 2a              	LDAA	SPDR,X	Get data from RTC into A (clears SPIF)
0179 0180 18 08              	INY		Advance data pointer
0180 0182 5a                 	DECB		Continue for B bytes
0181 0183 26 f0              	BNE	MOREW	Loop till B gets to zero
0182 0185 1d 08 20           	BCLR	PORTD,X SSPIN  Disable SS pin of RTC
0183 0188 38                 	PULX		Restore X
0184 0189 39                 	RTS		** Return from WBURST **
0185                         
0186                         
0187                         **************************************************************************
0188                         * RBURST  - Burst raed B bytes starting at addr A in RTC, uses Y index	 *
0189                         **************************************************************************
0190 018a 3c                 RBURST	PSHX		Save X for now
0191 018b ce 10 00           	LDX	#REGBAS Point to start of register block
0192 018e 1c 08 20           	BSET	PORTD,X SSPIN  Enable SS pin of RTC
0193 0191 a7 2a              	STAA	SPDR,X	Write Addr byte to RTC
0194 0193 1f 29 80 fc        	BRCLR	SPSR,X SPIF *  Wait for SPIF
0195 0197 a7 2a              MORER	STAA	SPDR,X	Initiate xfer to RTC (any data)
0196 0199 1f 29 80 fc        	BRCLR	SPSR,X SPIF *  Wait for SPIF
0197 019d a6 2a              	LDAA	SPDR,X	Get data from RTC into A (clears SPIF)
0198 019f 18 a7 00           	STAA	0,Y	Store new data byte
0199 01a2 18 08              	INY		Advance data pointer
0200 01a4 5a                 	DECB		Continue for B bytes
0201 01a5 26 f0              	BNE	MORER	Loop till B gets to zero
0202 01a7 1d 08 20           	BCLR	PORTD,X SSPIN  Disable SS pin of RTC
0203 01aa 38                 	PULX		Restore X
0204 01ab 39                 	RTS		** Return from RBURST **
0205                         
0206                         **************************************************************************
0207                         * DISPTIM - Display current time in the form HH:MM SS			 *
0208                         * CRLFTIM - alternate entry point to do CR,LF first			 *
0209                         **************************************************************************
0210 01ac bd ff c4           CRLFTIM JSR	OUTCRLF Send leading CR,LF to display
0211 01af 18 ce 00 30        DISPTIM LDY	#TIMTMP Point at 3 byte RAM holding area
0212 01b3 c6 03              	LDAB	#3	Request read of 3 bytes...
0213 01b5 86 20              	LDAA	#RTCSEC Starting with seconds address
0214 01b7 18 3c              	PSHY		Will need it again
0215 01b9 8d cf              	BSR	RBURST	Read in current time from MC68HC68T1
0216 01bb 38                 	PULX		Original value of Y now in X TIMTMP
0217                         * I don't really like this sequence but I need to accomodate the calling
0218                         * requirements of the Buffalo 3.2 routine "OUT1BYT".  Bytes are converted
0219                         * to two ASCII hex characters and displayed.  Data needs to be in the order
0220                         * it will be displayed and pointed-to by X.
0221 01bc a6 02              	LDAA	2,X	Data in wrong order, get Hours
0222 01be 84 1f              	ANDA	#$1F	Strip off 12 Hr coding bits
0223                         * This routine doesn't handle 24 Hr mode time (you would change $1F to $7F)
0224 01c0 e6 00              	LDAB	0,X	Get Seconds
0225 01c2 e7 02              	STAB	2,X
0226 01c4 a7 00              	STAA	0,X	Data now in correct order
0227 01c6 bd ff bb           	JSR	OUT1BYT Display hours (X moves to Minutes)
0228 01c9 86 3a              	LDAA	#':'    An ASCII colon
0229 01cb bd ff af           	JSR	OUTPUT	Display colon between Hr and Min
0230 01ce bd ff be           	JSR	OUT1BSP Display Minutes with trailing space
0231 01d1 bd ff bb           	JSR	OUT1BYT Display Seconds
0232 01d4 7e ff 7c           	JMP	WARMST	Go back to Buffalo Prompt ">"
0233                         
