REGBAS	equ	$1000

TIC1	equ	$10
TOC2	equ	$18
TCTL1	equ	$20
TCTL2	equ	$21
TFLG1	equ	$23
IC1F    equ     $4
OC2F    equ     $40

	org	$100

        ldx    #REGBAS
        ldaa   #$10
        staa   TCTL2,x
        clr    TCTL1,x

Loop:    ldx   #REGBAS
         bclr  TFLG1,x !IC1F
         brclr TFLG1,x IC1F *
         ldd   TIC1,x
         std   TOC2,x
         bclr  TFLG1,x !(IC1F+OC2F)
         brclr TFLG1,x IC1F+OC2F *
         brset TFLG1,x OC2F nofreq
         ldd   TIC1,x
         subd  TOC2,x
         cmpd  #32
         bls   toohigh
         ldx   #32
	 xgdx
         fdiv
         stx   freq
         ldd   freq           ; 2**20
         lsrd
         lsrd
         lsrd                 ; 2**17
         clr   temp
         staa  temp+1         ; a == 2**9
         lsrd
         lsrd
         std   temp1          ; 2**15
         ldd   freq           ; 2**20
         subd  temp1          ; -2**15
         addd  temp
         std   freq
         ldd   temp1          ; 2**15
         lsrd                 ; 2**14
         std   temp1
         ldd   freq
         subd  temp1
         std   freq
         ldaa  temp1
         staa  temp1+1        ; 2**6
         clr   temp1
         ldd   freq
         addd  temp1
         std   freq

         clr   temp
DLoop    inc   temp
         ldx   #10
         idiv
         pshb
         xgdx
         bne   DLoop

OLoop    pula
         adda  #'0
         jsr   $ffaf
         dec   temp
         bne   OLoop
         clr   $a6
         ldaa  #$0d
         jsr   $ffaf
         jmp   Loop

nofreq   jmp   Loop
toohigh  jmp   Loop

freq     rmb   2
temp     rmb   2
temp1    rmb   2
