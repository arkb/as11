AUTOLF	EQU	$A6
.OUTST0 EQU	$FFCA
.OUTPUT EQU	$FFAF

*****
* 68HC11 Register Equates
*****

REGBAS	EQU	$1000	Base address for register block of HC11

PORTD	EQU	$08	Port D (offset from REGBAS)
DDRD	EQU	$09	Data direction register for port D
SPCR	EQU	$28	SPI control register
SPSR	EQU	$29	SPI status register
SPDR	EQU	$2A	SPI data register

RTCRAM	EQU	$00	RAM in RTC is from $00 to $1F (32 bytes)
RTCSEC	EQU	$20	Two BCD digits 00-59
RTCMIN	EQU	$21	Two BCD digits 00-59
RTCHR	EQU	$22	Two BCD digits 01-12 or (00-23 if 24 hr mode)
AM	EQU	$80	Offset constant to set HRs to 12 hr AM
PM	EQU	$A0	Offset constant to set HRs to 12 hr PM
WEEKDAY EQU	$23	One BCD digit 01-07 (Sunday = 01)
MODATE	EQU	$24	Two BCD digits 01-31
MONTH	EQU	$25	Two BCD digits 01-12 (January = 01)
YEAR	EQU	$26	Two BCD digits 00-99
ALRMSEC EQU	$28	Two BCD digits 00-59
ALRMMIN EQU	$29	Two BCD digits 00-59
ALRMHR	EQU	$2A	Two BCD digits 01-12 or (00-23 if 24 hr mode)
RTCSTAT EQU	$30	RTC status register (read only)
RTCCTRL EQU	$31	RTC clock control register
RTCINT	EQU	$32	RTC interrupt control register

	org	$100

	CLR	AUTOLF
	ldx	#Str
	jsr	.OUTST0

	LDX	#REGBAS
	LDAA	#$00
	STAA	PORTD,X
	LDAA	#$38
	STAA	DDRD,X
	LDAA	#$54
	STAA	SPCR,X

*	LDAA	#$00
*	LDAB	 #RTCINT
*	BSR	 W1RTC

	LDAB	#RTCSEC
	CLRA
	JSR	W1RTC

	LDAB	#RTCMIN
	CLRA
	jsr	W1RTC

	LDAB	#RTCHR
	CLRA
	jsr	W1RTC

	LDAB	#WEEKDAY
	LDAA	#1
	bsr	W1RTC

	LDAB	#MODATE
	LDAA	#1
	bsr	W1RTC

	LDAB	#MONTH
	LDAA	#1
	bsr	W1RTC

	LDAB	#YEAR
	LDAA	#$90
	bsr	W1RTC

	LDAA	#$B0
	LDAB	#RTCCTRL
	bSR	W1RTC

Loop:	LDAB	#RTCSEC
	BSR	R1RTC
	LDY	#Sec
	bsr	B2A
	LDAB	#RTCMIN
	bsr	R1RTC
	LDY	#Min
	bsr	B2A
	LDAB	#RTCHR
	bsr	R1RTC
	LDY	#Hour
	bsr	B2A
	LDAB	#MODATE
	bsr	R1RTC
	LDY	#Day
	bsr	B2A
	LDAB	#MONTH
	bsr	R1RTC
	LDY	#Month
	bsr	B2A
	LDAB	#YEAR
	bsr	R1RTC
	LDY	#Year
	bsr	B2A

	ldab	#StrLen
BSLoop
	pshb
	ldaa	#8
	jsr	.OUTPUT
	pulb
	decb
	bpl	BSLoop

	ldx	#Str
	jsr	.OUTST0
	BRA	Loop

B2A	psha
	lsra
	lsra
	lsra
	lsra
	anda	#$f
	adda	#'0'
	staa	0,y
	iny
	pula
	anda	#$f
	adda	#'0'
	staa	0,y
	rts

W1RTC	ORAB	#$80	Set MSB of Addr byte to indicate write
R1RTC
	PSHX
	LDX	#REGBAS
	BSET	PORTD,X $20
	STAB	SPDR,X
	BRCLR	SPSR,X $80 *
	STAA	SPDR,X
	BRCLR	SPSR,X $80 *
	LDAA	SPDR,X
	BCLR	PORTD,X $20
	ANDB	#$7F
	PULX
	RTS

Str	equ	*
Day	FCC	'00-'
Month	FCC	'00-'
Year	FCC	'00 '
Hour	FCC	'00:'
Min	FCC	'00:'
Sec	FCC	'00'
StrLen	equ	*-Str
	FCB	$04

