**************************************************************************
* Software support routines for the MC68HC68T1 Real-Time-Clock/RAM & the *
* EVBU demonstration board.  Routines include...			 *
* RTCINIT - Subroutine to initialize Port D and SPI system.		 *
* SETDFLT - Subroutine to set up a default time 12:00:00 Mon 1/1/1989	 *
*	    and alarm set to same time but turned off.	RTC turned on.	 *
* SETTIME - Subroutine to set time sec/min/hr as pointed to by Y-reg	 *
* SETDATE - Subroutine to set date dow/dom/month/yr as pointed to by Y	 *
* SETALRM - Subroutine to set alarm time sec/min/hr as pointed to by Y	 *
* W1RTC   - Subroutine to write A data to addr B in RTC 		 *
* R1RTC   - Subroutine to read A data from addr B in RTC		 *
* WBURST  - Burst write B bytes starting at addr A in RTC, uses Y index  *
* RBURST  - Burst raed B bytes starting at addr A in RTC, uses Y index	 *
* DISPTIM - Display current time (alternate entry to do CR,LF first)	 *
**************************************************************************

*****
* 68HC11 Register Equates
*****

REGBAS	EQU	$1000	Base address for register block of HC11

PORTD	EQU	$08	Port D (offset from REGBAS)
SSPIN	EQU	$20	Port D bit 5 is SS* pin
DDRD	EQU	$09	Data direction register for port D
* 0,0,SS*,SCK;MOSI,MISO,TxD,RxD (RxD & TxD direction controlled by SCI)
* For EVBU w/ RTC use $38 -> SS,SCK,and MOSI=OUTS, rest=INS

SPCR	EQU	$28	SPI control register
* SPIE,SPE,DWOM,MSTR;CPOL,CPHA,SPR1,SPR0 (set to $54 for RTC interface)

SPSR	EQU	$29	SPI status register
* SPIF,WCOL,-,MODF;-,-,-,-
SPIF	EQU	$80	Mask for SPIF flag bit

SPDR	EQU	$2A	SPI data register

*****
* RTC Register Equates
*****

RTCRAM	EQU	$00	RAM in RTC is from $00 to $1F (32 bytes)

RTCSEC	EQU	$20	Two BCD digits 00-59
RTCMIN	EQU	$21	Two BCD digits 00-59
RTCHR	EQU	$22	Two BCD digits 01-12 or (00-23 if 24 hr mode)
*  Add $80 for 12 hr AM, Add $A0 for 12 hr PM
AM	EQU	$80	Offset constant to set HRs to 12 hr AM
PM	EQU	$A0	Offset constant to set HRs to 12 hr PM

WEEKDAY EQU	$23	One BCD digit 01-07 (Sunday = 01)
MODATE	EQU	$24	Two BCD digits 01-31
MONTH	EQU	$25	Two BCD digits 01-12 (January = 01)
YEAR	EQU	$26	Two BCD digits 00-99

ALRMSEC EQU	$28	Two BCD digits 00-59
ALRMMIN EQU	$29	Two BCD digits 00-59
ALRMHR	EQU	$2A	Two BCD digits 01-12 or (00-23 if 24 hr mode)
*  Add $80 for 12 hr AM, Add $A0 for 12 hr PM

RTCSTAT EQU	$30	RTC status register (read only)
* 0,WDOG,0,FRSTUP;INTHI,PWRSEN,ALRMINT,CLKINT

RTCCTRL EQU	$31	RTC clock control register
* START,LINE,XTLSEL1,XTLSEL0;50/60,CLKOUT2,CLKOUT1,CLKOUT0
* Set RTCCTRL = $B0 for Clk ON, 32KHz crystal, 60Hz, CLK OUT = Xtal

RTCINT	EQU	$32	RTC interrupt control register
* WDOGEN,PWRDWN,PWRSENE,ALRMEN;4-Bit PERIOD SELECT (see RTC data sheet)

*****
* Buffalo 3.2 Equates and Jump Table addresses
*****

USTACK	EQU	$47	Top of user stack area (0033-0047) Buffalo 3.2
TIMTMP	EQU	$30	3 bytes Temp holding locations for time display
WARMST	EQU	$FF7C	Warm Start
OUTPUT	EQU	$FFAF	Output ASCII in A-reg to EVBU terminal screen
OUT1BYT EQU	$FFBB	Convert & display byte pointed-to by X
OUT1BSP EQU	$FFBE	Like OUT1BYT with extra trailing space
OUTCRLF EQU	$FFC4	Output a <CR> and <LF>

* End of Equates
*****

	ORG	$0100	Upper half of 'E9 RAM

**************************************************************************
* RTCINIT - Subroutine to initialize Port D and SPI system.		 *
**************************************************************************
RTCINIT LDS	#USTACK Initialize stack pointer
	LDX	#REGBAS Point at start of register block
	LDAA	#$00
	STAA	PORTD,X Initial port D data
	LDAA	#$38	SS,SCK,and MOSI=OUTS, others=INS
	STAA	DDRD,X	Set directions for port D pins
	LDAA	#$54	SPIE,SPE,DWOM,MSTR;CPOL,CPHA,SPR1,SPR0
	STAA	SPCR,X	SPI On, Master, CPOL:CPHA=0:1, 1MHz
	RTS		** Return from RTCINIT **

**************************************************************************
* SETDFLT - Subroutine to set up a default time 12:00:00 Mon 1/1/1989	 *
*	    and alarm set to same time but turned off.	RTC turned on.	 *
* SETOTHR - This is alt entry point to set time to that pointed-to by Y  *
*	    Y must point to ordered set of 11 bytes (see DFLTSEC as Ex)  *
**************************************************************************
SETDFLT LDY	#DFLTSEC  Point at data to set RTC for defaults
SETOTHR LDAA	#RTCSEC Addr of first clock data loc in RTC
	LDAB	#11	Number of bytes to burst transfer to RTC
	BSR	WBURST	Do burst transfer of default settings
	LDAA	#$B0	RTC Clk ON, 32KHz crystal, 60Hz, CLK OUT = Xtal
	LDAB	#RTCCTRL  Address for RTC control reg
	BSR	W1RTC	Transfer init control word to start RTC
	LDAA	#$00	RTC Clk ON, 32KHz crystal, 60Hz, CLK OUT = Xtal
	LDAB	#RTCINT  Address for RTC interrupt control reg
	BSR	W1RTC	Turn off RTC interrupts
	RTS		** Return from SETDFLT or SETOTHR **

**************************************************************************
* SETTIME - Subroutine to set time sec/min/hr as pointed to by Y-reg	 *
**************************************************************************
SETTIME LDAA	#RTCSEC Address of first byte of burst Sec/Min/Hr
	LDAB	#3	Number of bytes to burst transfer
	BRA	BRSTOUT Go to common exit point

**************************************************************************
* SETDATE - Subroutine to set date dow/dom/month/yr as pointed to by Y	 *
**************************************************************************
SETDATE LDAA	#WEEKDAY  Address of 1st byte of burst WkDay/MoDat/Mo/Yr
	LDAB	#4	Number of bytes to burst transfer
	BRA	BRSTOUT Go to common exit point

**************************************************************************
* SETALRM - Subroutine to set alarm time sec/min/hr as pointed to by Y	 *
**************************************************************************
SETALRM LDAA	#ALRMSEC  Address of first byte of burst Sec/Min/Hr
	LDAB	#3	Number of bytes to burst transfer
BRSTOUT BSR	WBURST	Burst transfer to RTC
	RTS		** Return from SETALRM, SETDATE or SETTIME **

DFLTSEC FCB	$00,$00,$12+AM	12:00:00 AM
DFLTDAT FCB	$01,$01,$01,$89  Sun, 01/01/89
	FCB	$00	Place holder for unused loc $27 in RTC
DFLALRM FCB	$00,$00,$12+AM	12:00:00 AM


**************************************************************************
* W1RTC   - Subroutine to write A data to addr B in RTC 		 *
**************************************************************************
* R1RTC   - Subroutine to read A data from addr B in RTC		 *
**************************************************************************
W1RTC	ORAB	#$80	Set MSB of Addr byte to indicate write
R1RTC	PSHX		Save X for now
	LDX	#REGBAS Point to start of register block
	BSET	PORTD,X SSPIN  Enable SS pin of RTC
	STAB	SPDR,X	Write Addr byte to RTC
	BRCLR	SPSR,X SPIF *  Wait for SPIF
	STAA	SPDR,X	Initiate xfer of A to RTC
	BRCLR	SPSR,X SPIF *  Wait for SPIF
	LDAA	SPDR,X	Get data from RTC into A
	BCLR	PORTD,X SSPIN  Disable SS pin of RTC
	ANDB	#$7F	Restore original value to B
	PULX		Restore X
	RTS		** Return from W1RTC or R1RTC **

**************************************************************************
* WBURST  - Burst write B bytes starting at addr A in RTC, uses Y index  *
**************************************************************************
WBURST	ORAA	#$80	Set MSB of Addr byte to indicate write
	PSHX		Save X for now
	LDX	#REGBAS Point to start of register block
	BSET	PORTD,X SSPIN  Enable SS pin of RTC
	STAA	SPDR,X	Write Addr byte to RTC
	BRCLR	SPSR,X SPIF *  Wait for SPIF
MOREW	LDAA	0,Y	Get next data byte to transfer
	STAA	SPDR,X	Initiate xfer of A to RTC
	BRCLR	SPSR,X SPIF *  Wait for SPIF
	LDAA	SPDR,X	Get data from RTC into A (clears SPIF)
	INY		Advance data pointer
	DECB		Continue for B bytes
	BNE	MOREW	Loop till B gets to zero
	BCLR	PORTD,X SSPIN  Disable SS pin of RTC
	PULX		Restore X
	RTS		** Return from WBURST **


**************************************************************************
* RBURST  - Burst raed B bytes starting at addr A in RTC, uses Y index	 *
**************************************************************************
RBURST	PSHX		Save X for now
	LDX	#REGBAS Point to start of register block
	BSET	PORTD,X SSPIN  Enable SS pin of RTC
	STAA	SPDR,X	Write Addr byte to RTC
	BRCLR	SPSR,X SPIF *  Wait for SPIF
MORER	STAA	SPDR,X	Initiate xfer to RTC (any data)
	BRCLR	SPSR,X SPIF *  Wait for SPIF
	LDAA	SPDR,X	Get data from RTC into A (clears SPIF)
	STAA	0,Y	Store new data byte
	INY		Advance data pointer
	DECB		Continue for B bytes
	BNE	MORER	Loop till B gets to zero
	BCLR	PORTD,X SSPIN  Disable SS pin of RTC
	PULX		Restore X
	RTS		** Return from RBURST **

**************************************************************************
* DISPTIM - Display current time in the form HH:MM SS			 *
* CRLFTIM - alternate entry point to do CR,LF first			 *
**************************************************************************
CRLFTIM JSR	OUTCRLF Send leading CR,LF to display
DISPTIM LDY	#TIMTMP Point at 3 byte RAM holding area
	LDAB	#3	Request read of 3 bytes...
	LDAA	#RTCSEC Starting with seconds address
	PSHY		Will need it again
	BSR	RBURST	Read in current time from MC68HC68T1
	PULX		Original value of Y now in X TIMTMP
* I don't really like this sequence but I need to accomodate the calling
* requirements of the Buffalo 3.2 routine "OUT1BYT".  Bytes are converted
* to two ASCII hex characters and displayed.  Data needs to be in the order
* it will be displayed and pointed-to by X.
	LDAA	2,X	Data in wrong order, get Hours
	ANDA	#$1F	Strip off 12 Hr coding bits
* This routine doesn't handle 24 Hr mode time (you would change $1F to $7F)
	LDAB	0,X	Get Seconds
	STAB	2,X
	STAA	0,X	Data now in correct order
	JSR	OUT1BYT Display hours (X moves to Minutes)
	LDAA	#':'    An ASCII colon
	JSR	OUTPUT	Display colon between Hr and Min
	JSR	OUT1BSP Display Minutes with trailing space
	JSR	OUT1BYT Display Seconds
	JMP	WARMST	Go back to Buffalo Prompt ">"

