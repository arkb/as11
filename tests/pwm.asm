*****************************************
*	Pulse-Width Modulation Software *
*					*
*	Uses OC1 for rising edges and	*
*	OC2 for falling enges		*
*****************************************

*************************
*	Equations	*
*************************

IOBank	equ	$1000

PORTA	equ	$00
CFORC	equ	$0b
OC1M	equ	$0c
OC1D	equ	$0d
TCNT	equ	$0e
TIC1	equ	$10

TOC1	equ	$16
TOC2	equ	$18
TOC4	equ	$1c
TCTL1	equ	$20
TCTL2	equ	$21
TMSK1	equ	$22
TFLG1	equ	$23

PACTL	equ	$26
PACNT	equ	$27

ADCTL	equ	$30
ADR1	equ	$31
ADR2	equ	$32
ADR3	equ	$33
ADR4	equ	$34

HPRIO	equ	$3c

OC4Vect equ	$d6
OC2Vect equ	$dc
OC1Vect equ	$df

*************************
*	Data section	*
*************************
	org	$0

Time	RMB	2		Time
NewTime RMB	2		New time
Per8	RMB	2		Period/8
Per16	RMB	2		Period/16

*************************
*	Code section	*
*************************
		org	$100

		opt	c

		SEI
		LDAA	#$7e			"JMP" opcode
		STAA	OC1Vect 		Save it
		LDD	#HandlOC1		Handler routine address
		STD	OC1Vect+1		Save it

		LDX	#IOBank 		Init IX

		CLRA
		STAA	PORTA,x 		Reset OC2/PA6 pin
		LDAA	#%00010000		IC1 on rising edges
		STAA	TCTL2,x

		LDAA	#$40			OC1 drives PA6
		STAA	OC1D,x
		STAA	OC1M,x
		LDAA	#%10000000		; OC2 clear pin mode
		STAA	TCTL1,x


* Start A/D convertions exactly at "time" 0
		LDD	#-12-32			; Next section takes 12 ticks
		STD	TOC1,x
		BCLR	TFLG1,x $ff-%10000000	; Reset OC1 flag
		BRCLR	TFLG1,x %10000000 *	; Wait for event
		LDAA	#%00100001		; One channel, continuously
		STAA	ADCTL,x

		LDD	#250			; 1 kHz / 8
		STD	Per8
		LSRD
		ADDD	#$80			; ADC adjust
		STD	Per16

		BCLR	TFLG1,x $ff-%100	; Reset IC1 flag
		BRCLR	TFLG1,x %00000100 *	; Wait for event
		LDD	TIC1,x			; Get "time"
		STD	Time
		STD	TOC1,x			; Init TOC1 (dummy)

* Enable OC1 interrupts
		LDAA	#%11000100
		STAA	TFLG1,x 		; Reset OC1, OC2 & IC1 flags
		LDAA	#%10000000
		STAA	TMSK1,x 		; Enable OC1 interrupts
		CLI

* Main loop
Loop:		BRCLR	TFLG1,x %00000100 *	; Wait until IC1
		LDAA	#%01000100		; Reset IC1 & OC2 flags
		STAA	TFLG1,x

* Force first pulse
		LDD	TIC1,x			; Read "time" of edge
		ADDD	#37			; Next section takes some time
		STD	TOC1,x			; Edge time for OC1
		ADDD	Per16
		STD	TOC2,x

* Store input pulse time & calculate periods
		LDD	TIC1,x
		STD	NewTime
		SUBD	Time			; New period
		LSRD
		LSRD
		LSRD
		ADCB	#0			; More precious period / 8
		ADCA	#0
		STD	Per8
		LSRD
		ADCB	#0			; Correct precision
		ADCA	#0
		ADDD	#$80			; ADC adjust
		STD	Per16

		LDD	NewTime
		STD	Time			; For next pass

		LDY	#7			; 7 pulses
Loop1		LDD	TOC1,x			; Previous "time"
		ADDD	Per8			; Next "time"

		BRCLR	TFLG1,x %01000000 *	; Wait for OC2
		STD	TOC1,x
		ADDD	Per16			; Next "time" for OC2
		STD	TOC2,x
		LDAA	#%01000000		; Reset OC2 flag
		STAA	TFLG1,x
		DEY
		BNE	Loop1

* Last pulse - don't start OC1
		BRCLR	TFLG1,x %01000000 *	; Wait for OC2
		BRA	Loop

* OC1 interrupt hanler
HandlOC1	LDAB	TCNT+1,x		; Least byte of "time"
		ADDB	#20			; Next section takes such time

* Bits 5 & 6 say which ADR register contains the most recent result
		RORB
		RORB
		RORB
		RORB
		ANDB	#%11			; Isolate bits 5 & 6
		ABX
		LDD	TOC2+IOBank		; Get approx. OC2 "time"
		SUBB	ADR1,x			; Correct it by ADC result
		SBCA	#0			; Correct carry
		STD	TOC2+IOBank		; Store it
		LDAA	#%10000000		; Reset OC1 flag
		STAA	TFLG1+IOBank
		RTI

