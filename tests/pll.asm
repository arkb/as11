*****************************************
*	Phase-Locked Loop Software	*
*****************************************

*************************
*	Equations	*
*************************

KOEFF	equ	5

IOBank	equ	$1000

PORTA	equ	$00
CFORC	equ	$0b
OC1D	equ	$0d
TCNT	equ	$0e
TIC1	equ	$10
TOC2	equ	$18
TCTL1	equ	$20
TCTL2	equ	$21
TMSK1	equ	$22
TFLG1	equ	$23

OC2Vect equ	$dc

*************************
*	Data section	*
*************************
	org	$0

Time	RMB	2		Time
NewTime RMB	2		New time
EndTime RMB	2		Expected time of next edge
Interv	RMB	2		Number of intervals
LastPer RMB	2

*************************
*	Code section	*
*************************
	org	$100

	SEI					Disable interrupts
*	 LDS	 #Stack
	LDAA	#$7e				"JMP" opcode
	STAA	OC2Vect 			Save it
	LDD	#Handl				Handler routine address
	STD	OC2Vect+1			Save it

	LDX	#IOBank 			Init IX
	CLRA
	STAA	PORTA,x 			Reset OC2/PA6 pin
	LDAA	#%00010000			IC1 on rising edges
	STAA	TCTL2,x
	BCLR	TFLG1,x $ff-%100		Reset IC1 flag

	BRCLR	TFLG1,x %00000100 *		Wait until first edge
	LDD	TIC1,x				Get this time
	STD	Time
	BCLR	TFLG1,x $ff-%100		Reset IC1 flag
	BRCLR	TFLG1,x %00000100 *		Wait until second edge
	BCLR	TFLG1,x $ff-%100		Reset IC1 flag
	LDD	TIC1,x				Second edge
	SUBD	Time				Period

* Calculate next edge time
	STD	EndTime 			Period
	LDD	TIC1,x				Current time
	STD	Time				Store it
	ADDD	EndTime 			Plus period
	STD	EndTime 			Expected next moment

* Enable OC2 interrupts
	LDAA	#%01000000			OC2 toggle pin mode
	STAA	TCTL1,x
	STAA	TFLG1,x 			Reset OC2 flag
	STAA	TMSK1,x 			Enable OC2 interrupts

* Force OC2
	LDD	#2*KOEFF			Number of output edges
	STD	Interv
	LDAA	#%01000000
	STAA	CFORC,x
	CLI
	LDD	#2*KOEFF-1			Number of output edges

	opt	c

Loop:	STD	Interv				Initialize number of intervals
	BCLR	TFLG1,x $ff-%100		Reset IC1 flag
	BRCLR	TFLG1,x %00000100 *		Wait until IC1
	LDD	TIC1,x				Read time of edge
	STD	NewTime
	SUBD	Time				New period
	ADDD	NewTime 			Time of next edge
	STD	EndTime 			Save next edge time
	LDD	NewTime
	STD	Time
	LDD	#2*KOEFF
	BRCLR	PORTA,x %01000000 Loop

* First interval already started
	LDD	#2*KOEFF-1
	BRA	Loop

Handl:	LDD	EndTime 			Expected end time
	SUBD	TOC2,x				Subtract current time
	LDX	Interv
	BEQ	Handl1
	IDIV
	XGDX
	STD	LastPer 			Save calculated period
	LDX	#IOBank
	ADDD	TOC2,x				+ current time
	STD	TOC2,x				= next time
	DEC	Interv+1
	BCLR	TFLG1,x $ff-%01000000		Clear OC2 flag
	RTI					End of handler

* Input signal disapeared
Handl1	LDD	LastPer
	LDX	#IOBank
	ADDD	TOC2,x
	STD	TOC2,x
	BCLR	TFLG1,x $ff-%01000000		Clear OC2 flag
	RTI					End of handler

	opt	noc

*Stack	 RMB	 1
