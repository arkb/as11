//  do.c
//
void
localinit(void);

void
do_op(
    int     opcode,
    int     class);

int
bitop(
    int     op,
    int     mode,
    int     class);

void
do_gen(
    int     op,
    int     mode,
    int     pnorm,
    int     px,
    int     py);

void
do_indexed(
    int     op);

void
epage(
    int     p);

//  eval.c
//
int
eval(void);

//  ffwd.c
//
void
fwdinit(void);

void
fwdreinit(void);

void
fwdmark(void);

void
fwdnext(void);

//  output.c
//
void
stable(
    struct nlist *ptr);
void
cross(
    struct nlist *point);

//  pseudo.c
//
void
do_pseudo(
    int     op);

//  symtab.c
//
int
install(
    char    *str,
    int     val);

struct nlist *
lookup(
    char    *name);

struct oper *
mne_look(
    char    *str);

//  util.c
//
void
fatal(
    char    *str);

void
error(
    char    *str);

void
warn(
    char    *str);

void
suggest(
    char    *str);

int
delim(
    char    c);

char *
skip_white(
    char    *ptr);

void
eword(
    int     wd);

void
emit(
    int     byte);

void
f_record(void);

void
hexout(
    int     byte);

void
print_line(void);

int
any(
    char    c,
    char    *str);

char
mapdn(
    char    c);

int
lobyte(
    int     i);

int
hibyte(
    int     i);

int
head(
    char    *str1,
    char    *str2);

int
alpha(
    char    c);

int
alphan(
    char    c);

char *
alloc(
    int     nbytes);

/* EOF */

