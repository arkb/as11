#ifndef WIN32
    FILE    *fopen();
    char    *fgets();
#endif

// Forward declarations
//
void
initialize(void);

void
re_init(void);

void
make_pass(void);

int
parse_line(void);

void
process(void);

void
usage(void);

void
version(void);

/*
 *  as ---  cross assembler main program
 */
int
main(
    int     argc,
    char    **argv)
{
    char    **np;
    int     j = 0;

    Argv = argv;
    
    if (argc < 2)
    {
        usage();
        exit(1);
    }
    
    while ((j < argc) &&
            (*argv[j] != '-'))
    {
        j++;
    }

    N_files = j - 1;

    if (N_files > 0)
    {
        initialize();
    }

    while (j < argc)
    {
		// Skip the leading '-'.
		//
		argv[j]++;

		if (strcmp(argv[j], "l") == 0)
        {
            Lflag = 1;
        }
        else if (strcmp(argv[j], "nol") == 0)
        {
            Lflag = 0;
        }
        else if (strcmp(argv[j], "c") == 0)
        {
            Cflag = 1;
        }
        else if (strcmp(argv[j], "noc") == 0)
        {
            Cflag = 0;
        }
        else if (strcmp(argv[j], "s") == 0)
        {
            Sflag = 1;
        }
        else if (strcmp(argv[j], "cre") == 0)
        {
            CREflag = 1;
        }
        else if (strcmp(argv[j], "v") == 0)
        {
            version();
            exit(1);
        }
        else if (strcmp(argv[j], "h") == 0)
        {
            usage();
            exit(1);
        }

        j++;
    }

    if (N_files == 0)
    {
        usage();
        exit(1);
    }
 
    root = NULL;

    Cfn = 0;
    np = argv;

    while (++Cfn <= N_files)
    {
        Line_num = 0; /* reset line number */
        strcpy(Current_file, *np);
        if ((Fd = fopen(*++np, "r")) == NULL)
        {
            printf("as: can't open %s\n", *np);
        }
        else
        {
            make_pass();
            fclose(Fd);
            Fd = NULL;
        }
    }

    if (Err_count == 0)
    {
        Pass++;
        re_init();
        Cfn = 0;
        np = argv;
        while (++Cfn <= N_files)
        {
            if ((Fd = fopen(*++np,"r")) != NULL)
            {
                Line_num = 0;
                strcpy(Current_file, *np);
                make_pass();
                fclose(Fd);
                Fd = NULL;
            }
        }

        if (Sflag == 1)
        {
            printf ("\f");
            stable (root);
        }

        if (CREflag == 1)
        {
            printf ("\f");
            cross (root);
        }

        fprintf(Objfil, "S9030000FC\n"); /* at least give a decent ending */
        fclose(Objfil);
        Objfil = NULL;
    }

	fprintf(stderr, "Errors: %d\n", Err_count);

	exit(Err_count);
}

/*
 *
 */
void
initialize(void)
{
#ifdef DEBUG
    printf("Initializing\n");
#endif

    Err_count = 0;
    Pc = 0;
    Pass = 1;
    Lflag = 0;
    Cflag = 0;
    Ctotal = 0;
    Sflag = 0;
    CREflag = 0;
    N_page = 0;
    Line[sizeof(Line) - 1] = EOS;

    strcpy(Obj_name,Argv[1]); /* copy first file name into array */

    // Find the last '.' and truncate the file name.
    //
    int i;
    for (i = strlen(Obj_name) - 1; i >= 0; --i)
    {
        if (Obj_name[i] == '.')
        {
           Obj_name[i] = 0;
           break;
        }
    }

    if (i < 0)
    {
        fatal("Failed to construct the output (.s19) file name since there is no extension in the input file name");
    }

    // Append the output extension to the file name.
    //
    strcat(Obj_name, ".s19");

    Objfil = fopen(Obj_name, "w");
    if (Objfil == NULL)
    {
        fatal("Can't create object file");
    }

    fwdinit();  /* forward ref init */
    localinit();    /* target machine specific init. */
}

/*
 *
 */
void
re_init(void)
{
#ifdef DEBUG
    printf("Reinitializing\n");
#endif
    Pc = 0;
    E_total = 0;
    P_total = 0;
    Ctotal = 0;
    N_page = 0;

    fwdreinit();
}

/*
 *
 */
void
make_pass(void)
{
#ifdef DEBUG
    printf("Pass %d\n", Pass);
#endif

    while (fgets(Line, sizeof(Line), Fd) != NULL)
    {
        Line_num++;
        P_force = 0;    /* No force unless bytes emitted */
        N_page = 0;

        // Ensure that we do not cross the buffer boundary.
        //
        Line[sizeof(Line) - 1] = EOS;

        // Find EOL and 0-terminate the input line at that position.
        //
        for (char* p = Line; *p != EOS; ++p)
        {
            // Treat \r, \n, ^Z and ^D characters as EOLs.
            //
            if (*p == '\r' || *p == '\n' || *p == '\x1a' || *p == '\x04')
            {
                *p = EOS;
                break;
            }
        }

        if (parse_line())
        {
            process();
        }

        if (Pass == 2 && Lflag && !N_page)
        {
            print_line();
        }

        P_total = 0;    /* reset byte count */
        Cycles = 0; /* and per instruction cycle count */
    }

    f_record();
}


/*
 *  parse_line --- split input line into label, op and operand
 */
int
parse_line(void)
{
    register char   *ptrfrm = Line;
    register char   *ptrto = Label;

    // Ignore empty lines or lines starting with '*' (comments).
    //
    if (*ptrfrm == EOS || *ptrfrm == '*')
    {
        return 0;
    }

    if (*ptrfrm == '#')
    {
        if (!strncmp(ptrfrm + 1, "line", 4))
        {
            ptrfrm += 4;
        }

        /* preprocessor line */
        if (sscanf(ptrfrm + 1, "%d %s", &Line_num, Current_file) != 2)
        {
            error("Illegal preprocessor line\n");
        }

        Line_num--;
        return 0;
    }

    if (delim(*ptrfrm) == NO)
    {
        while (delim(*ptrfrm)== NO)
        {
            *ptrto++ = *ptrfrm++;
        }

        if (*--ptrto != ':')
        {
            ptrto++;     /* allow trailing : */
        }
    }

    *ptrto = EOS;

    ptrfrm = skip_white(ptrfrm);

    ptrto = Op;
    while (delim(*ptrfrm) == NO)
    {
        *ptrto++ = mapdn(*ptrfrm++);
    }

    *ptrto = EOS;

    ptrfrm = skip_white(ptrfrm);

    ptrto = Operand;
    while (*ptrfrm != EOS)
    {
        *ptrto++ = *ptrfrm++;
    }

    *ptrto = EOS;

#ifdef DEBUG
    printf("Label-%s-\n", Label);
    printf("Op----%s-\n", Op);
    printf("Operand-%s-\n", Operand);
#endif

    return 1;
}

/*
 *  process --- determine mnemonic class and act on it
 */
void
process(void)
{
    register struct oper    *i;
    struct oper             *mne_look();

    Old_pc = Pc;        /* setup `old' program counter */
    Optr = Operand;     /* point to beginning of operand field */

    if (*Op == EOS)
    {
        /* no mnemonic */
        if (*Label != EOS)
        {
            install(Label, Pc);
        }
    }
    else if ((i = mne_look(Op)) == NULL)
    {
        error("Unrecognized Mnemonic");
    }
    else if (i->class == PSEUDO)
    {
        do_pseudo(i->opcode);
    }
    else
    {
        if (*Label)
        {
            install(Label, Pc);
        }

        if (Cflag)
        {
            Cycles = i->cycles;
        }

        do_op(i->opcode, i->class);
        
        if (Cflag)
        {
            Ctotal += Cycles;
        }
    }
}

/*
 *
 */
void
usage(void)
{
    version();
    printf("\n");
    printf("Usage:\n");
    printf("  %s [files]\n", Argv[0]);
}

/*
 *
 */
void
version(void)
{
    printf("6811 assembler 2014+ (Version %d.%d)\n", VERSION_MAJOR, VERSION_MINOR);
    printf("  original program by Motorola.\n");
    printf("  few modifications by Randy Sargent (rsargent@media.mit.edu)\n");
    printf("  subsequent modifications by Arkadi Brjazovski (as11@brjazovski.com)\n");
}

/* EOF */
