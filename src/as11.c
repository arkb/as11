// Uncomment DEBUG below to enavle execution traces.
//
//#define DEBUG 1

#if defined(WIN32)
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#if defined(WIN32)

#include <io.h>

#else

#include <fcntl.h>
#include <unistd.h>

#endif

#include "as.h"
#include "table11.h"
#include "as.c"
#include "do11.c"
#include "pseudo.c"
#include "eval.c"
#include "symtab.c"
#include "util.c"
#include "ffwd.c"
#include "output.c"

/* EOF */

