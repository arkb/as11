/* ROM BIOS Serial Interface Constants */

/* Bit Rates */

#define B1200   0x80
#define B2400   0xA0
#define B4800   0xC0
#define B9600   0xE0

/* Parity */
#define PNONE   0

/* Stop bits */
#define SB1     0
#define SB2     4

/* Data bits */

#define DB8     3

/* Line status */
#define TIMEOUT 0x8000 /* Time out */
#define TSRE    0x4000 /* Transmit Shift reg empty*/
#define THRE    0x2000
#define BREAKDET    0x1000
#define FRAMERR 0x800
#define PARERR  0x400
#define OVERERR 0x200
#define RCVDRDY 0x100

/* modem status */
#define RLSD    0x80
#define RI      0x40
#define DSR     0x20
#define CTS     0x10
#define DRLST   8
#define TERD    4
#define DDSR    2
#define DCTS    1

/* EOF */

