#!/usr/bin/env bash

. ./common.sh

getOS || exit $?

getVersion || exit $?

echo "Processing package version $VERSION for $OS"

FILES=Changes.txt
case $OS in
    win32)
        for file in src/win32/Debug/as11.{exe,pdb}
        do
            FILES="$FILES $file"
        done
        ;;

    linux | macos)
        FILES="$FILES src/as11"
        ;;

    *)  
        checkError 1 'Internal script error!' || exit $?
        ;;
esac

PACKAGE=as11-$OS-$VERSION.zip
ZIP="zip -j9 $PACKAGE"
for file in $FILES
do
    if ! [ -e $file ]
    then
        checkError 1 "FAILURE: File $file does not exist. Did your forget to build it?" || exit $?
    fi

    ZIP="$ZIP $file"
done

echo "Creating a package: $ZIP"
$ZIP
checkError $? "FAILURE: Failed to create a package $PACKAGE"
exit $?
